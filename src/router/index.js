import { createRouter, createWebHistory } from 'vue-router'
import Home from '../components/Home.vue'
import MyMotus from '../components/MyMotus.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/motus',
    name: 'MyMotus',
    component: MyMotus,
  }
]
const router = createRouter({
  history: createWebHistory(),
  routes,
})
export default router