import { ApolloClient, HttpLink, InMemoryCache, split, from } from '@apollo/client/core'
import { WebSocketLink } from "@apollo/client/link/ws";
import { createApolloProvider } from '@vue/apollo-option'
import { getMainDefinition } from '@apollo/client/utilities';
import { onError } from '@apollo/client/link/error';

const httpLink = new HttpLink({
  uri: `${process.env.NODE_ENV === 'development' ? `http://${window.location.hostname}:8080` : window.location.protocol + '//' + window.location.hostname}/graphql`,
  fetch: (uri, options) => {
    const { operationName } = JSON.parse(options.body);
    return fetch(`${uri}?opname=${operationName}`, options);
  }
});

// Create the subscription websocket link
const wsLink = new WebSocketLink({
  uri: process.env.NODE_ENV === 'development' ? 
    `ws://${window.location.hostname}:8080/subscriptions` :
    `wss://${window.location.hostname}/subscriptions`,
  options: {
    reconnect: true,
    reconnectionAttempts: 10,
    timeout: 55000
  },
});

const errorLink = onError(({ response, graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      console.error(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      ));
  }
  if (networkError) console.error(`[Network error]: ${networkError}`);
  if (response) response.errors = null;
});

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  httpLink
)

// Create the apollo client
const apolloClient = new ApolloClient({
  link: from([
    errorLink,
    link
  ]),
  cache: new InMemoryCache(),
  connectToDevTools: true,
})
const apolloProvider = createApolloProvider({
  defaultClient: apolloClient,
})

export default apolloProvider;