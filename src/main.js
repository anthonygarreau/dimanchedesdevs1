import { createApp } from 'vue'
import App from './App.vue'
import router from "./router/index"
import './index.css'
import apolloProvider from './apollo-client'

createApp(App)
  .use(router)
  .use(apolloProvider)
  .mount('#app')
