const IORedis = require('ioredis');
const db = process.env.NODE_ENV === 'development' ? '0' : '0';
const REDIS_DB = `${process.env.REDIS_URL}/${db}`;
const redisOptions = {
  // tls: { rejectUnauthorized: false }
}
const redis = process.env.NODE_ENV === 'production' ? new IORedis(REDIS_DB, redisOptions) : new IORedis();

module.exports = redis;