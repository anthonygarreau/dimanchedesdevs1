const { GraphQLScalarType } = require('graphql');

module.exports = new GraphQLScalarType({
  name: 'Void',
  description: 'Represents NULL values',
  serialize() {
    return null;
  },
  parseValue() {
    return null;
  },
  parseLiteral() {
    return null;
  }
});
