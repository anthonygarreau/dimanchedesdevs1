const dictionnary = require('./dictionnary.json')

module.exports = ({ roundNumber, word: givenWord, totalGuesses }) => {
  const word = givenWord || dictionnary[Math.floor(Math.random() * dictionnary.length)];

  console.log(word) // A supp?
  return {
    roundNumber,
    word,
    firstLetter: word[0],
    wordLength: word.length,
    guessNumber: 0,
    guesses: [],
    state: 'IN_PROGRESS',
    totalGuesses
  };
}
