const pubsub = require('../../pubsub')

module.exports = {
  Subscription: {
    onTry: {
      subscribe: () => pubsub.asyncIterator('ON_TRY'),
      resolve: payload => payload
    }
  }
}