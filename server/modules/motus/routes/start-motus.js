const { uid } = require("uid");
const generateRound = require('../generate-round');
const redis = require('../../../ioredis');

module.exports = async (req, res) => {
  const gameId = req.params.teamName + uid(3);
  const roundNumber = 0;
  const round = generateRound({ roundNumber, totalGuesses: 0 });
  const { firstLetter, wordLength, word } = round; // public fields
  
  await redis.set(
    `motus:${gameId}:round:${roundNumber}`,
    JSON.stringify(round),
    'EX', 360  // 10mn
  );

  redis.set(
    `motus:${gameId}`,
    JSON.stringify({
      gameId,
      rounds: [{
        roundNumber: 0,
        firstLetter,
        wordLength,
        word,
        totalGuesses: 0
      }]
    }),
    'EX', 1200 // 20mn
  )

  const game = {
    gameId,
    roundNumber: 0,
    firstLetter,
    wordLength,
    totalGuesses: 0
  };

  return res.json(game);
}