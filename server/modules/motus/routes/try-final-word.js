const { uid } = require("uid");
const generateRound = require('../generate-round');
const redis = require('../../../ioredis');
const dictionnary = require('../dictionnary.json');
const selectedWords = require('../selected-words.json');
const finalWords1 = require('../selected-words1.json');
const finalWords2 = require('../selected-words2.json');
const finalWords3 = require('../selected-words3.json');

const finalWords = [finalWords1, finalWords2, finalWords3];

const VALIDATION = {
  CORRECT: 'correct',
  INCORRECT: 'incorrect',
  MISPLACED: 'misplaced'
}

module.exports = async (req, res) => {
  const gameId = req.params.gameId;
  const roundNumber = req.params.roundNumber;
  const word = req.params.word.toUpperCase();
  
  const rawRound = await redis.get(`motus:${gameId}:round:${roundNumber}`);
  const round = JSON.parse(rawRound);

  if (!round) {
    return res.status(400).json({ error: 'ROUND_NOT_FOUND' });
  } else if (round.roundNumber >= 499) {
    return res.status(400).json({ res: 'GAME_WON' });
  } else if (round.state === 'WON') {
    return res.status(400).json({ error: 'ROUND_ALREADY_WON' });
  } else if (!dictionnary.includes(word)) {
    return res.status(400).json({ error: 'UNKNOWN_WORD' });
  } else if(word.length !== round.wordLength) {
    return res.status(400).json({ error: 'INVALID_WORD_LENGTH' });
  } else if(word[0] !== round.firstLetter) {
    return res.status(400).json({ error: 'INVALID_FIRST_LETTER' });
  } else if(round.guessNumber >= 6) {
    return res.status(400).json({ error: 'ROUND_ALREADY_LOST' });
  }

  // IF WORD FOUND WOUHOU generate new round ! 
  if (round.word === word) {
    const newRoundNumber = parseInt(round.roundNumber) + 1;
    const newTotalGuesses = parseInt(round.totalGuesses) + 1;
    const newRound = generateRound({ roundNumber: newRoundNumber, word: finalWords[process.env.DATASET][newRoundNumber], totalGuesses: newTotalGuesses });
    const { firstLetter, wordLength, word } = newRound; // public fields // TODO word is not public its for debugging purpose
    await redis.set(
      `motus:${gameId}:round:${newRoundNumber}`,
      JSON.stringify(newRound),
      'EX', 360  // 10mn
    );

    // Push last word in current round
    round.guesses.push({ word });
    round.totalGuesses++;
    round.state = 'WON';
    redis.set(
      `motus:${gameId}:round:${round.roundNumber}`,
      JSON.stringify(round),
      'EX', 360  // 10mn
    );

    redis.publish('ON_TRY', JSON.stringify({
      command: 'NEW_ROUND',
      gameId,
      roundNumber: newRoundNumber,
      firstLetter,
      wordLength,
      state: 'IN_PROGRESS',
      totalGuesses: round.totalGuesses
    }))

    return res.json({
      command: 'NEW_ROUND',
      gameId,
      roundNumber: newRoundNumber,
      firstLetter,
      wordLength,
      totalGuesses: round.totalGuesses
    });
  } else {
    // Word not found, check validation and update round
    const inputWordArray = word.split('');
    const roundWordArray = round.word.split('');

    // find red letters
    let validation = inputWordArray.map((letter, index) => {
      if (letter === roundWordArray[index]) {
        return VALIDATION.CORRECT;
      }
      return VALIDATION.WRONG;
    });

    // remove red letter from available letters
    const numberByLetter = roundWordArray.reduce((acc, letter) => {
      if (!acc[letter]) acc[letter] = 0;
      acc[letter]++;
      return acc;
    }, {});

    validation.forEach((color, index) => {
      if (color === VALIDATION.CORRECT) numberByLetter[inputWordArray[index]]--;
    })

    // find yellow letters
    validation = validation.map((color, index) => {
      if (color === VALIDATION.CORRECT) {
        return VALIDATION.CORRECT;
      }
      if (
        color !== VALIDATION.CORRECT &&
        roundWordArray.includes(inputWordArray[index]) &&
        numberByLetter[inputWordArray[index]] > 0
      ) {
        numberByLetter[inputWordArray[index]]--;
        return VALIDATION.MISPLACED;
      }
      return VALIDATION.INCORRECT;
    });

    // TODO add try to round and write in redis
    round.guessNumber++;
    round.totalGuesses++;
    round.guesses.push({
      word: word,
      validation
    });

    redis.set(
      `motus:${gameId}:round:${round.roundNumber}`,
      JSON.stringify(round),
      'EX', 360  // 10mn
    );
  
    // if is not correct, and it was the last attempt (6)
    // End game
    if (round.guessNumber === 6) {
      redis.publish('ON_TRY', JSON.stringify({
        command: 'LOST_ROUND',
        gameId,
        roundNumber: round.roundNumber,
        firstLetter: round.firstLetter,
        wordLength: round.wordLength,
        guesses: round.guesses,
        totalGuesses: round.totalGuesses
      }))

      return res.json({
        command: 'LOST_ROUND',
        guesses: round.guesses,
        totalGuesses: round.totalGuesses
      });
    }

    redis.publish('ON_TRY',  JSON.stringify({
      command: 'TRY_WORD',
      gameId,
      roundNumber: round.roundNumber,
      firstLetter: round.firstLetter,
      wordLength: round.wordLength,
      guesses: round.guesses,
      totalGuesses: round.totalGuesses
    }))

    return res.json({
      command: 'TRY_WORD',
      gameId,
      roundNumber: round.roundNumber,
      firstLetter: round.firstLetter,
      wordLength: round.wordLength,
      guesses: round.guesses,
      totalGuesses: round.totalGuesses
    });
  }
}