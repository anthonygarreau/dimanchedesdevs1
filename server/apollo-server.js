const { ApolloServer } = require('apollo-server-express');
const {
  ApolloServerPluginDrainHttpServer,
  ApolloServerPluginLandingPageGraphQLPlayground
} = require('apollo-server-core');
const { SubscriptionServer } = require('subscriptions-transport-ws');
const { execute, subscribe } = require('graphql');
const { makeExecutableSchema } = require('graphql-tools');
const path = require('path');
const glob = require('glob');
const { readFileSync } = require('fs');
const redis = require('./ioredis');
const { merge } = require('lodash');
const defaultTypeDefs = require('./default-types');
const Void = require('./scalar-void');

const readFilesInDir = (dirPath) => {
  return new Promise((resolve, reject) => {
    glob(dirPath, { nosort: true, nodir: true }, function (err, files) {
      if (err) reject(err);
      resolve(files);
    });
  });
};

module.exports = async ({ httpServer }) => {
  let resolvers = {
    Void
  };
  let permissions = {};
  const schemas = [];
  
  const graphFiles = await readFilesInDir(path.join(
    __dirname,
    './modules/*/{schema.gql,resolvers.js,permissions.js}'
  ));
  for (const graphFile of graphFiles) {
    try {
      if (graphFile.endsWith('schema.gql')) {
        schemas.push(readFileSync(graphFile, { encoding: 'utf8' }));
      } else if (graphFile.endsWith('resolvers.js')) {
        resolvers = merge(resolvers, require(graphFile));
      } else if (graphFile.endsWith('permissions.js')) {
        permissions = merge(permissions, require(graphFile)(permissionsRules, { and, or, rule }));
      }
    } catch (err) {
      console.warn(err);
    }
  }

  try {
    const schema = makeExecutableSchema({
      resolvers: resolvers,
      typeDefs: [defaultTypeDefs, ...schemas]
    });
  
    const subscriptionServer = SubscriptionServer.create({
      schema,
      execute,
      subscribe,
      keepAlive: 50000
    }, {
      server: httpServer,
      path: '/subscriptions',
    });

    const apolloServer = new ApolloServer({
      playground: true,
      schema,
      context: async ({ req }) => {
        return {
          redis,
          req
        };
      },
      plugins: [
        ApolloServerPluginLandingPageGraphQLPlayground({
          // options
        }),
        ApolloServerPluginDrainHttpServer({ httpServer }),
        {
          async serverWillStart() {
            return {
              async drainServer() {
                subscriptionServer.close();
              }
            };
          }
        }
      ],
      subscriptions: {
        path: '/subscriptions'
      }
    });
  
    return apolloServer;
  } catch (err) {
    console.error(err);
  }
};