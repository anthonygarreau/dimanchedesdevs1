const dico = require('./server/modules/motus/dictionnary.json');
const fs = require('fs');

function pick1000randomWords() {
    const words = [];
    for (let i = 0; i < 1000; i++) {
        words.push(dico[Math.floor(Math.random() * dico.length)]);
    }
    return words;
}


fs.writeFileSync('server/modules/motus/selected-words1.json', JSON.stringify(pick1000randomWords()));
fs.writeFileSync('server/modules/motus/selected-words2.json', JSON.stringify(pick1000randomWords()));
fs.writeFileSync('server/modules/motus/selected-words3.json', JSON.stringify(pick1000randomWords()));
