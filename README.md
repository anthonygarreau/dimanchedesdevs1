# README

First, get the dictionary [here](https://we.tl/t-2SH4naEBJM)

The goal of this game is to resolve 500 "MOTUS" words in a row.

There's an API at your disposal. Use it to launch a new game and try words until you manage to resolve 500 rounds.

# API

## **Launch a new game**

**URL** : `/api/new-game/<team-name>`

**Method** : `GET`

**Example**

```json
/api/new-game/myteam
```

### **Success Response**

**Code** : `200 OK`

**Content example**

```json
  { 
    "gameId": "myteam123",
    "roundNumber": 0,
    "firstLetter": "E",
    "wordLength": 8
  }
```

## **Try a word**

**URL** : `/api/game/<team-name>/round/<round-number>/guess/<word-guess>`

**Method** : `GET`

**Example**

```json
/api/game/myteam123/round/0/guess/ESCALOPE
```

### **Success Responses**

**Code** : `200 OK`

**Condition** : If the round is not won.

**Content example**

```json
  {
    "command": "TRY_WORD",
    "gameId": "myteam123",
    "roundNumber": 0,
    "firstLetter": "E",
    "wordLength": 8,
    "guesses": [{
      "word": "ESCALOPE",
      "validation": ["correct", "correct", "incorrect", "incorrect", "misplaced", "incorrect",  "misplaced", "incorrect"]
    }]
  }
```

**Condition** : If the round is won.

**Content example**

```json
  {
    "command": "NEW_ROUND",
    "gameId": "myteam123",
    "roundNumber": 1,
    "firstLetter": "A",
    "wordLength": 9
  }
```

**Condition** : If the game is won.

**Content example**

```json
  { 
    "res": "GAME_WON"
  }
```

### **Error Responses**

**Condition** : If the word is not in the dictionary.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "error": "UNKNOWN_WORD"
}
```

**Condition** : If the word guessed don't have the correct length.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "error": "INVALID_WORD_LENGTH"
}
```

**Condition** : If you've already won this round.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "error": "ROUND_ALREADY_WON"
}
```

**Condition** : If the round number is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
  "error": "ROUND_NOT_FOUND"
}
```
