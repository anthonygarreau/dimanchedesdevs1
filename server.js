const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const http = require('http');
const serveStatic = require("serve-static")
const logger = require('morgan');
const indexRouter = require('./server/routes');
const createApolloServer = require('./server/apollo-server.js');
const newGame = require('./server/modules/motus/routes/start-motus.js');
const tryWord = require('./server/modules/motus/routes/try-word.js');
const newGameFinale = require('./server/modules/motus/routes/start-final-motus.js');
const tryWordFinale = require('./server/modules/motus/routes/try-final-word.js');

async function start() {
  const app = express();

  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(serveStatic(path.join(__dirname, 'dist')));
  app.get('/', (req, res) => {
    res.sendFile('dist/index.html');
  });

  app.use('/', indexRouter);
  app.get('/api/new-game/:teamName', newGame);
  app.get('/api/game/:gameId/round/:roundNumber/guess/:word', tryWord);
  app.get('/api/finale/new-game/:teamName', newGameFinale);
  app.get('/api/finale/game/:gameId/round/:roundNumber/guess/:word', tryWordFinale);

  const port = process.env.PORT || 8080;
  const httpServer = http.createServer(app);
  const server = await createApolloServer({ httpServer });

  await server.start();

  server.applyMiddleware({ app });
  await new Promise(resolve => httpServer.listen({ port }, resolve));
  console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);
}

start(); 
